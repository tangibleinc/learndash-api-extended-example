# Learndash Api Extended Example

## Requirement

[Node.js](https://nodejs.org/en/) must be installed, with its package manager, `npm`.

## Install

This repo is meant to be a starting point for a local test site of your own.

Clone, rename the folder, and remove existing `.git` folder.

```sh
git clone git@bitbucket.org:tangibleinc/learndash-api-extended-example.git

mv learndash-api-extended-example learndash-api-extended-test
cd learndash-api-extended-test
rm -rf .git
```

If you'd like, run `git init` to start a new repo.

Then install dependencies.

```sh
npm install
```

## Configure

The app configuration is found in `src/App/config.js`.

In particular, the variable `requestOptions` holds an array of test request options, which will be available on the frontend.

Each option has the properties `method`, `route`, and `data`. Add here the requests you'd like to test.

## Use

#### Develop

Build during development - watch files and rebuild.

```sh
npm run dev
```

The command will start a static file server at `http://localhost:3000`.

There's a file watcher that will reload the browser page when CSS/JS is rebuilt.

#### Build

Build for production - minified script and styles.


```sh
npm run build
```
