import { useRef } from 'react'
import {
  defaultSiteUrl,
  getOAuthTokenUrl
} from './config'
import {
  login
} from './api'
import {
  resetPersistentState
} from './state'

const AuthScreen = ({
  persistentState, setPersistentState,
  sessionState, setSessionState
}) => {

  const {
    siteUrl,
    clientId,
    clientSecret,
    username,
    password
  } = persistentState

  const {
    statusMessage,
    sessionTokenReceived
  } = sessionState

  const submitData = data => {

    setPersistentState(data)

    setSessionState({
      statusMessage: 'Sending..'
    })

    const requestStatus =
      'URL: '+getOAuthTokenUrl(siteUrl)+'\nRequest:\n'+JSON.stringify(data, null, 2)

    login(data)
      .then(response => {

        setSessionState({
          statusMessage: requestStatus+'\nResponse:\n'+JSON.stringify(response, null, 2)
        })

        if (response && response.access_token) {
          setSessionState({
            sessionTokenReceived: response.access_token
          })
        }
      })
      .catch(error => {
        setSessionState({
          statusMessage: requestStatus+'\nError: '+error.message
        })
      })
  }

  const formRef = useRef(null)

  return <form ref={formRef} onSubmit={e => {

    e.preventDefault()

    const data = Object.fromEntries(
      new FormData(e.target).entries()
    )

    if (!data.siteUrl) data.siteUrl = defaultSiteUrl

    submitData(data)
  }}>

    <p>
      <label>API site URL</label>&nbsp;
      <input name="siteUrl" type="text" defaultValue={siteUrl} required />
    </p>
    <p>
      <label>Client ID</label>&nbsp;
      <input name="clientId" type="text" defaultValue={clientId} required />
    </p>
    <p>
      <label>Client Secret</label>&nbsp;
      <input name="clientSecret" type="password" defaultValue={clientSecret} required />
    </p>
    <p>
      <label>Test user name</label>&nbsp;
      <input name="username" type="text" defaultValue={username} required />
    </p>
    <p>
      <label>Test user password</label>&nbsp;
      <input name="password" type="password" defaultValue={password} required />
    </p>
    <hr/>

    <button type="submit" style={{ float: 'left' }}>Authenticate</button>&nbsp;

    {/* <button type="button" style={{ float: 'right' }} onClick={() => {

      formRef.current && formRef.current.reset()
      setPersistentState(
        resetPersistentState()
      )
    }}>Clear settings</button>
    <br/> */}

    <pre><code>{ statusMessage }</code></pre>

    { sessionTokenReceived &&
      <button type="button" onClick={() => {
        setPersistentState({
          sessionToken: sessionTokenReceived
        })
        setSessionState({
          statusMessage: '',
          sessionTokenReceived: false
        })
      }}>Proceed to next screen</button>
    }
  </form>
}

export default AuthScreen