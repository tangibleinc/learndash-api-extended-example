import { useMemo } from 'react'
import {
  apiUrl,
  requestOptions,
} from './config'
import {
  createApi
} from './api'

const APIExplorer = ({
  persistentState, setPersistentState,
  sessionState, setSessionState
}) => {

  const {
    siteUrl,
    sessionToken
  } = persistentState

  const api = useMemo(() => createApi(siteUrl, sessionToken), [sessionToken])

  const {
    statusMessage,
    currentRequestOptionIndex
  } = sessionState

  const setOptionIndex = index => setSessionState({
    currentRequestOptionIndex: index
  })

  const sendRequest = () => {

    const request = requestOptions[ currentRequestOptionIndex ]
    if (!request) {
      setSessionState({
        statusMessage: 'Request option '+currentRequestOptionIndex+' not found'
      })
      return
    }

    setSessionState({
      statusMessage: 'Sending..'
    })

    const requestStatus =
      'URL: '+(api.url || 'Unknown')+'\nRequest:\n'+JSON.stringify(request, null, 2)

    api(request)
      .then(response => {
        setSessionState({
          statusMessage: requestStatus+'\nResponse:\n'+JSON.stringify(response, null, 2)
        })
      })
      .catch(error => {
        setSessionState({
          statusMessage: requestStatus+'\nError: '+error.message
        })
      })
  }

  return <form onSubmit={e => {
    e.preventDefault()
    sendRequest()
  }}>

    <p><code>{ api.url }</code></p>

    { requestOptions.map((req, i) =>
      <div key={i}>
        <label htmlFor="currentRequestOptionIndex" style={{ display: 'flex' }}
          onClick={() => setOptionIndex(i)}
        >
          <input name="currentRequestOptionIndex" type="radio" value={i} checked={
            currentRequestOptionIndex===i
          } onChange={e => {
            const { value } = e.target
            setOptionIndex(parseInt(value, 10))
          }} />
          &nbsp;
          <pre><code>{ JSON.stringify(req, null, 2) }</code></pre>
        </label>
      </div>
    )}
    <hr/>

    <button type="submit" style={{ float: 'left' }}>Send</button>

    <button type="button" style={{ float: 'right' }} onClick={() => {
      setSessionState({
        statusMessage: ''
      })
      setPersistentState({
        sessionToken: ''
      })
    }}>Logout</button>

    <br/>
    <pre><code>{ statusMessage }</code></pre>
  </form>
}


export default APIExplorer