
// Config

export const defaultSiteUrl = 'https://dev.tangibledash.com/'
export const getApiUrl = siteUrl => `${siteUrl}wp-json/`
export const getOAuthTokenUrl = siteUrl => `${siteUrl}oauth/token/`

// Test requests

export const requestOptions = [
  { method: 'GET', route: 'ldlms/v1/courses/25503', data: {} }
]
