import { useState, useRef } from 'react'
import {
  defaultSiteUrl
} from './config'

// State

export const loadPersistentState = () => {
  let state

  try {
    state = JSON.parse(
      localStorage.getItem('appState')
    )
  } catch(e) { /* OK */ }

  return state && typeof state==='object'
    ? state
    : {
      siteUrl: defaultSiteUrl,
      clientId: '',
      clientSecret: '',
      username: '',
      password: '',
      sessionToken: ''
    }
}

export const savePersistentState = state => localStorage.setItem('appState', JSON.stringify(state))

export const resetPersistentState = () => {
  localStorage.removeItem('appState')
  return loadPersistentState() // Return init state
}

export const initPersistentState = loadPersistentState()

// Wrap React.useState to log current state
export const usePersistentState = initState => {

  const [state, _setState] = useState(initState)
  const setState = statePartial => {

    // Ensure trailing slash
    if (statePartial.siteUrl && statePartial.siteUrl[ statePartial.siteUrl.length -1 ]!=='/') {
      statePartial.siteUrl += '/'
    }

    const newState = {
      ...state,
      ...statePartial
    }

    console.log('app.state', newState)

    savePersistentState(newState)
    _setState(newState)
  }

  // For convenience - control app from dev console
  window.app = Object.assign(window.app || {}, {
    state, setState
  })

  return [state, setState]
}

// Per-session state

export const initSessionState = {
  statusMessage: '',
  sessionTokenReceived: false,
  currentRequestOptionIndex: 0,
}

export const useSessionState = initState => {

  const [state, _setState] = useState(initState)
  const ref = useRef(state)
  const setState = statePartial => {

    const newState = {
      ...ref.current,
      ...statePartial
    }

    // Keep track of fresh state, because React useState doesn't set it immediately
    ref.current = newState

    console.log('app.sessionState', newState)

    _setState(newState)
  }

  // For convenience - inspect and control app from dev console
  window.app = Object.assign(window.app || {}, {
    sessionState: state,
    setSessionState: setState
  })

  return [state, setState]
}
