import React from 'react'
import {
  usePersistentState,
  initPersistentState,
  useSessionState,
  initSessionState
} from './state'
import APIExplorer from './APIExplorer'
import AuthScreen from './AuthScreen'

const App = () => {

  const [persistentState, setPersistentState] = usePersistentState(initPersistentState)
  const [sessionState, setSessionState] = useSessionState(initSessionState)

  const screenProps = {
    persistentState, setPersistentState,
    sessionState, setSessionState
  }

  return <div>
    { persistentState.sessionToken
      ? <APIExplorer {...screenProps} />
      : <AuthScreen {...screenProps} />
    }
  </div>
}

export default App