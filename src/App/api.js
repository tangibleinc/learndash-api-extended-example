import {
  getApiUrl,
  getOAuthTokenUrl
} from './config'

// Wrap fetch for API requests

export const createApi = (siteUrl, sessionToken) => {

  const apiUrl = getApiUrl(siteUrl)

  async function api({
    route,
    method,
    data
  }) {

    const url = apiUrl + route

    const request = {
      method: method.toUpperCase(),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer '+sessionToken
      }
    }

    // Prepare data for sending

    if (method==='GET') {

      // Data for GET method must be encoded as query string

      const query = Object.keys(data)
        .map(k => `${k}=${encodeURIComponent(data[k])}`)
        .join('&')

      route += (route.indexOf('?') < 0 ? '?' : '') + query

    } else {
      request.body = JSON.stringify(data)
    }

    console.log('api', url, request)

    const result = await fetch(url, request)

    return await result.json()
  }

  api.url = apiUrl

  return api
}

export const login = async ({
  siteUrl,
  username, password,
  clientId, clientSecret
}) => {

  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      grant_type: 'password',
      username,
      password,
      client_id: clientId,
      client_secret: clientSecret
    })
  }

  console.log('login', request)

  const result = await fetch(getOAuthTokenUrl(siteUrl), request)

  return await result.json()
}
